from django.urls import path


from meal_plans.views import (
    MealPlanListView,
    MealPlanCreateView,
    MealPlanDetailView,
    MealPlanEditView,
    MealPlanDeleteView,
)

urlpatterns = [
    path("", MealPlanListView.as_view(), name="mealplan_list"),
    path("create/", MealPlanCreateView.as_view(), name="mealplan_create"),
    path("<int:pk>/detail/", MealPlanDetailView.as_view(), name="mealplan_details"),
    path("<int:pk>/edit/", MealPlanEditView.as_view(), name="mealplan_edit"),
    path("int:pk>/delete/", MealPlanDeleteView.as_view(), name="mealplan_delete"),
        ]
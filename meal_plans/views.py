from .models import MealPlan
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import (ListView, CreateView, DetailView, UpdateView,
        DeleteView)
from django.urls import reverse_lazy


# Create your views here.

class MealPlanListView(LoginRequiredMixin, ListView):
    model = MealPlan
    template_name = "meals/list.html"

    # def get_queryset(self):
    #     return MealPlan.objects.filter(owner=self.request.user)


class MealPlanDetailView(LoginRequiredMixin, DetailView):
    model = MealPlan
    template_name = "meals/detail.html"


class MealPlanCreateView(LoginRequiredMixin, CreateView):
    model = MealPlan
    template_name = "meals/create.html"
    fields = ["name", "date", "recipes"]
    success_url = reverse_lazy("mealplan_list")

    #  def form_valid(self, form):
    #     form.instance.owner = self.request.user
    #     return super().form_valid(form)


class MealPlanEditView(LoginRequiredMixin, UpdateView):
    model = MealPlan
    template_name = "meals/edit.html"
    fields = ["name", "description", "date", "recipes"]
    success_url = reverse_lazy("mealplan_list")


class MealPlanDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlan
    template_name = "meals/delete.html"
    success_url = reverse_lazy("mealplan_list")

